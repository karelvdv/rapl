#define _XOPEN_SOURCE 700 // for getline
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <time.h>
#include <errno.h>
#include <unistd.h>
#include "rapl.h"
#include "serial.h"

/**
 *	Get the current UTC time in milliseconds.
 */
static uint64_t mtime(struct timeval* tv)
{
	gettimeofday(tv, NULL);
	return 1000ULL * (uint64_t)tv->tv_sec + (uint64_t)tv->tv_usec / 1000ULL;
}

/**
 *	Sleep for the given amount of milliseconds.
 */
static void msleep(uint32_t ms)
{
	struct timespec req = {
		.tv_sec = ms / 1000,
		.tv_nsec = 1000000 * (ms % 1000),
	};
	// continue sleep in case it is interrupted.
	while( (nanosleep(&req, &req) == -1) && (errno == EINTR) ) continue;
	if(errno) perror("nanosleep");
}

/**
 *	Stress the CPU for a couple of seconds.
 */
static void stress(uint32_t ncpu, uint32_t secs)
{
	char cmdbuf[32];
	FILE* p;

	sprintf(cmdbuf, "stress -c %u -t %u", ncpu, secs);

	if( (p = popen(cmdbuf, "r")) == NULL )
	{
		perror("popen:stress");
		fprintf(stderr, "stress command not found.\n");
		exit(1);
	}

	pclose(p);
}

/**
 *	rapl info [-cN]
 */
static int cmd_info(int argc, char** argv)
{
	rapl_package rapl;
	rapl_info info;
	uint32_t cpu = 0;
	int c;

	while( (c = getopt(argc, argv, "c:")) != -1 )
	{
		switch(c)
		{
		case 'c':
			cpu = (uint32_t)atoi(optarg);
			break;
		default:
			fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
			return 1;
		}
	}

	rapl_open(&rapl, cpu);
	rapl_getinfo(&rapl, &info);

	printf("power_thermal_spec: %.3fW\n", info.tdp);
	printf("power_min: %.3fW\n", info.power_min);
	printf("power_max: %.3fW\n", info.power_max);
	printf("time_window: %.6fs\n", info.time_window);
	printf("power_limit_1: %.3fW\n", info.power_limit_1);
	printf("power_limit_2: %.3fW\n", info.power_limit_2);
	printf("time_window_1: %.6fs\n", info.time_window_1);
	printf("time_window_2: %.6fs\n", info.time_window_2);
	printf("pl_lock: %d\n", info.pl_lock );
	printf("pl1_enable: %d\n", info.pl1_enable );
	printf("pl2_enable: %d\n", info.pl2_enable );
	printf("pl1_clamp: %d\n", info.cl1_enable );
	printf("pl2_clamp: %d\n", info.cl2_enable );
	printf("dram power_thermal_spec: %.3fW\n", info.dram.tdp);
	printf("dram power_min: %.3fW\n", info.dram.power_min);
	printf("dram power_max: %.3fW\n", info.dram.power_max);
	printf("dram time_window: %.6fs\n", info.dram.time_window);

	rapl_close(&rapl);
	return 0;
}

#define MAXSOCKETS 256
#define MAXCPU 1024

/**
 *	rapl read [-h] [-iM] [-oF] [-S] cmd...
 */
static int cmd_read(int argc, char** argv)
{
	static rapl_package rapls[MAXSOCKETS];
	static rapl_state states[2 * MAXSOCKETS];
	static cputopo_entry topology[MAXCPU];
	serial_handle ser = SERIAL_INVALID;
	uint32_t interval = 1000;
	int printheader = 0;
	int dostress = 0;
	FILE* output = stderr;

	int c;
	while( (c = getopt(argc, argv, "hi:o:s:S")) != -1 )
	{
		switch(c)
		{
		case 'i':
			interval = atoi(optarg);
			break;
		case 'h':
			printheader = 1;
			break;
		case 'o':
			output = fopen(optarg, "w");
			break;
		case 's':
			if( (ser = serial_open(optarg)) == SERIAL_INVALID)
			{
				fprintf(stderr, "serial port error.\n");
				return 1;
			}
			serial_setrts(ser, 0);
			break;
		case 'S':
			dostress = 1;
			break;
		case '?':
		default:
			fprintf(output, "Unknown option character `\\x%x'.\n", optopt);
			return 1;
		}
	}

	if(optind == argc)
	{
		fprintf(output, "Missing command to execute.\n");
		return 1;
	}

	// open one rapl for each cpu socket.
	uint32_t ncpus = lscpu(topology, MAXCPU);
	uint32_t nsockets = 1;
	uint32_t last_socket = 0;
	rapl_open(&rapls[0], 0);

	for(uint32_t i = 1; i < ncpus; i++)
	{
		const cputopo_entry* entry = &topology[i];

		if(entry->socket > last_socket)
		{
			rapl_open(&rapls[nsockets++], entry->logical_id);
			last_socket = entry->socket;
		}
	}

	memset(states, 0, sizeof(states));

	if(printheader)
	{
		fprintf(output, "time,temp_ia32,");
		for(uint32_t i = 0; i < nsockets; i++)
		{
			fprintf(output, "pkg_%u,pp0_%u,pp1_%u,dram_%u,temp_%u", i, i, i, i, i);
			if( i < (nsockets - 1) ) fprintf(output, ",");
		}
		fprintf(output, "\n");
	}

	if(dostress)
	{
		stress(ncpus - 1, 1);
	}

	pid_t child = fork();

	// child process executes command.
	if(child == 0)
	{
		dup2( fileno(stdout), fileno(stderr) ); // redir stderr to stdout
		msleep(interval);
		execvp(argv[optind], argv + optind);
		exit(-1); // only executed if execvp fails.
	}
	// parent process reads rapls until child process terminates.
	else
	{
		char timebuf[80];
		struct timeval tv;

		int flip = 0;
		rapl_state* statebuf[2] = {states + 0, states + nsockets};
		uint64_t time[2] = { 0 };

		// set initial state.
		time[1] = mtime(&tv);
		for(uint32_t i = 0; i < nsockets; i++)
		{
			rapl_read(&rapls[i], &statebuf[1][i]);
		}

		msleep(interval);

		// set serial rts
		if(ser != SERIAL_INVALID) serial_setrts(ser, 1);

		do
		{
			time[flip] = mtime(&tv);

			uint64_t elapsed = time[flip] - time[flip ^ 1];
			double s_elapsed = (double)elapsed * 1e-3;

			uint32_t temp_ia32 = rapl_temperature_ia32(&rapls[0]);

			// print current time and temperature.
			strftime(timebuf, 80, "%Y-%m-%d %H:%M:%S", localtime(&tv.tv_sec));
			fprintf(output, "%s.%03u,%u,", timebuf, (uint32_t)(tv.tv_usec / 1000), temp_ia32);

			for(uint32_t i = 0; i < nsockets; i++)
			{
				rapl_state* last = &statebuf[flip ^ 1][i];
				rapl_state* now  = &statebuf[flip][i];

				rapl_read(&rapls[i], now);
				double pkg  = (now->energy_pkg  - last->energy_pkg)  / s_elapsed;
				double pp0  = (now->energy_pp0  - last->energy_pp0)  / s_elapsed;
				double pp1  = (now->energy_pp1  - last->energy_pp1)  / s_elapsed;
				double dram = (now->energy_dram - last->energy_dram) / s_elapsed;
				uint32_t temp = rapl_temperature(&rapls[i]);

				fprintf(output, "%.3f,%.3f,%.3f,%.3f,%u", pkg, pp0, pp1, dram, temp);
				if( i < (nsockets - 1) ) fprintf(output, ",");
			}

			fprintf(output, "\n");
			fflush(output);

			flip ^= 1;
			msleep(interval);
			// continue until child process status changes.
			} while( waitpid(child, NULL, WNOHANG | WCONTINUED) == 0 );
	}

	if(dostress)
	{
		stress(ncpus - 1, 1);
	}

	if(ser != SERIAL_INVALID)
	{
		serial_setrts(ser, 0);
		serial_close(ser);
	}

	for(uint32_t i = 0; i < nsockets; i++)
	{
		rapl_close(&rapls[i]);
	}

	if(output != stderr)
	{
		fclose(output);
	}

	return 0;
}

static void usage()
{
	printf("rapl info [-cN]\n");
	printf("Shows basic RAPL information.\n");
	printf("  -cN Read information from Nth cpu. [def=0]\n");
	printf("rapl read [-h] [-iM] [-oF] [-sD] [-S] cmd...\n");
	printf("Reads and displays time series of power measurements for the duration of the command.\n");
	printf("The time series is written to stderr and the command output to stdout.\n");
	printf("  -h  Print time series header.\n");
	printf("  -iM Read interval in M milliseconds. [def=1000]\n");
	printf("  -oF Output to file F [def=stderr].\n");
	printf("  -sD Open serial device D and set RTS for the duration of cmd [def=no serial].\n");
	printf("  -S  Stress CPU for 1 second before and after cmd is executed.\n");
	printf("  cmd Specifies the command to execute, including any parameters.\n");
	printf("Example: rapl read -h -i500 sleep 10\n");
}

int main(int argc, char** argv)
{
	if(argc >= 2)
	{
		char* cmd = argv[1];

		if( !strcmp(cmd, "info") )
		{
			return cmd_info(argc - 1, argv + 1);
		}
		else if( !strcmp(cmd, "read") )
		{
			return cmd_read(argc - 1, argv + 1);
		}
		else
		{
			fprintf(stderr, "Invalid command: %s\n", cmd);
			return 1;
		}
	}
	else
	{
		usage();
	}

	return 0;
}
