#pragma once
#include <stdint.h>
#include "msr.h"

typedef struct rapl_info
{
	// socket rapl info
	double tdp; //W
	double power_min; //W
	double power_max; //W
	double time_window; //s
	double power_limit_1; //W
	double time_window_1; //s
	double power_limit_2; //W
	double time_window_2; //s

	// dram info
	struct
	{
		double tdp;
		double power_min;
		double power_max;
		double time_window;
	} dram;

	// rapl flags
	struct
	{
		uint64_t pl_lock : 1;
		uint64_t pl1_enable : 1;
		uint64_t cl1_enable : 1;
		uint64_t pl2_enable : 1;
		uint64_t cl2_enable : 1;
	};
} rapl_info;

typedef struct rapl_state
{
	double energy_dram; // J
	double energy_pkg;
	double energy_pp0;
	double energy_pp1;
} rapl_state;

typedef struct rapl_package
{
	double units_power;  // W
	double units_energy; // J
	double units_time;   // s
	uint32_t cpu_model;
	msr_handle msr;
} rapl_package;

void rapl_open(rapl_package* rapl, uint32_t logical_cpu);
void rapl_close(rapl_package* rapl);
void rapl_getinfo(const rapl_package* rapl, rapl_info* info);
void rapl_read(const rapl_package* rapl, rapl_state* state);
uint32_t rapl_temperature(const rapl_package* rapl);
uint32_t rapl_temperature_ia32(const rapl_package* rapl);

typedef struct cputopo_entry
{
	uint32_t logical_id;
	uint32_t physical_id;
	uint32_t socket;
} cputopo_entry;

uint32_t lscpu(cputopo_entry* topology, uint32_t topology_max);
