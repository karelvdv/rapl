CC     := gcc
CFLAGS := -Wall -O2 -std=gnu99
LIBS   := -lm
SRC    := rapl.c msr.c main.c serial.c
EXE    := rapl
UNAME  := $(shell uname)

ifeq ($(UNAME), Linux)
	SETCAP := sudo setcap cap_sys_rawio=ep $(EXE)
else
	SETCAP :=
endif

all: $(EXE) setcap

$(EXE): $(SRC)
	$(CC) $(CFLAGS) $(SRC) -o $(EXE) $(LIBS)

setcap: $(EXE)
	$(SETCAP)

clean:
	rm -f $(EXE)
