# Linux RAPL Tool
Author: Karel van der Veldt  
License: ISC

## Description

It's a tool to generate time series data from Intel's RAPL power management interface.

## Setup

1. sudo modprobe msr
2. sudo chmod o+r,o+w /dev/cpu/*/msr
3. make