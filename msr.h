#pragma once
#include <stdint.h>
#include <sys/types.h>

typedef int msr_handle;

msr_handle msr_open(uint32_t cpu);
void msr_close(msr_handle fd);
uint64_t msr_read(msr_handle fd, off_t which);
int msr_write(msr_handle fd, off_t which, uint64_t value);
