/**
 *	Code based on source by Vince Weaver:
 *	http://web.eece.maine.edu/~vweaver/projects/rapl/
 **/

#define _XOPEN_SOURCE 700 // for getline
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <errno.h>
#include <unistd.h>
#include "msr.h"
#include "rapl.h"

#define MSR_RAPL_POWER_UNIT      0x606

/*
 * Platform specific RAPL Domains.
 * Note that PP1 RAPL Domain is supported on 062A only
 * And DRAM RAPL Domain is supported on 062D only
 */
// Package RAPL Domain
#define MSR_PKG_RAPL_POWER_LIMIT 0x610
#define MSR_PKG_ENERGY_STATUS    0x611
#define MSR_PKG_PERF_STATUS      0x613
#define MSR_PKG_POWER_INFO       0x614

// PP0 RAPL Domain
#define MSR_PP0_POWER_LIMIT      0x638
#define MSR_PP0_ENERGY_STATUS    0x639
#define MSR_PP0_POLICY           0x63A
#define MSR_PP0_PERF_STATUS      0x63B

// PP1 RAPL Domain, may reflect to uncore devices
#define MSR_PP1_POWER_LIMIT      0x640
#define MSR_PP1_ENERGY_STATUS    0x641
#define MSR_PP1_POLICY           0x642

// DRAM RAPL Domain
#define MSR_DRAM_POWER_LIMIT     0x618
#define MSR_DRAM_ENERGY_STATUS   0x619
#define MSR_DRAM_PERF_STATUS     0x61B
#define MSR_DRAM_POWER_INFO      0x61C

// RAPL UNIT BITMASK
#define POWER_UNIT_OFFSET        0x00
#define POWER_UNIT_MASK          0x0F

#define ENERGY_UNIT_OFFSET       0x08
#define ENERGY_UNIT_MASK         0x1F00

#define TIME_UNIT_OFFSET         0x10
#define TIME_UNIT_MASK           0xF000

// thermal status
#define MSR_PACKAGE_THERM_STATUS 0x01B1
#define MSR_IA32_THERM_STATUS    0x019C

/*
 *	Supported families and models.
 */
#define CPU_FAMILY_SUPPORTED 6

#define CPU_SANDYBRIDGE     42
#define CPU_SANDYBRIDGE_EP  45
#define CPU_IVYBRIDGE       58
#define CPU_IVYBRIDGE_EP    62
#define CPU_HASWELL         60

static const uint32_t supported_models[] = {
	CPU_SANDYBRIDGE,
	CPU_SANDYBRIDGE_EP,
	CPU_IVYBRIDGE,
	CPU_IVYBRIDGE_EP,
	CPU_HASWELL,
	0,
};

typedef union cpuid_info
{
	struct { int32_t eax, ebx, ecx, edx; };
	struct { int32_t : 32; char vendor_string[12]; };
} cpuid_info;

static inline void cpuid(int leaf, cpuid_info* info)
{
	__asm__ __volatile__ ("cpuid" : \
		"=a" (info->eax), "=b" (info->ebx), "=c" (info->ecx), "=d" (info->edx) : "a" (leaf));
}

static uint32_t detect_cpu(void)
{
	cpuid_info info;
	cpuid(0, &info);

	if( memcmp(info.vendor_string, "GenuntelineI", 12) != 0 )
	{
		fprintf(stderr, "Unsupported architecture. Only Intel architectures are supported.\n");
		exit(1);
	}

	cpuid(1, &info);
	uint32_t cpu_family = ((info.eax >> 8) & 0xf) | ((info.eax & 0xf00000) >> 16);
	uint32_t cpu_model  = ((info.eax & 0xf0) >> 4) | ((info.eax & 0xf0000) >> 12);

	if(cpu_family != CPU_FAMILY_SUPPORTED)
	{
		fprintf(stderr, "Unsupported CPU family: %d\n", cpu_family);
		exit(1);
	}

	const uint32_t* supported = supported_models;
	while( *supported && (*supported != cpu_model) ) supported++;

	if(*supported == 0)
	{
		fprintf(stderr, "Unsupported CPU model: %d\n", cpu_model);
		exit(1);
	}

	return cpu_model;
}

uint32_t lscpu(cputopo_entry* topology, uint32_t topology_max)
{
	char* linebuf = malloc(256);
	size_t linebuf_len = 256;
	uint32_t cpu_count = 0;
	FILE* p;

	if( (p = popen("lscpu -p", "r")) == NULL )
	{
		perror("popen:lscpu");
		fprintf(stderr, "lscpu command not found\n");
		exit(1);
	}

	while( !feof(p) && (cpu_count < topology_max) )
	{
		(void)getline(&linebuf, &linebuf_len, p);
		if(linebuf[0] == '#') continue;
		cputopo_entry* entry = &topology[cpu_count];
		sscanf(linebuf, "%u,%u,%u,", &entry->logical_id, &entry->physical_id, &entry->socket);
		cpu_count++;
	}

	pclose(p);
	free(linebuf);
	return cpu_count;
}

void rapl_open(rapl_package* rapl, uint32_t logical_cpu)
{
	rapl->cpu_model = detect_cpu();
	rapl->msr = msr_open( logical_cpu );
	assert(rapl->msr >= 0);

	uint64_t result    = msr_read(rapl->msr, MSR_RAPL_POWER_UNIT);
	rapl->units_power  = pow( 0.5, (double)(result & 0xf)) ;
	rapl->units_energy = pow( 0.5, (double)((result >> 8) & 0x1f) );
	rapl->units_time   = pow( 0.5, (double)((result >> 16) & 0xf) );
}

void rapl_close(rapl_package* rapl)
{
	msr_close(rapl->msr);
}

void rapl_getinfo(const rapl_package* rapl, rapl_info* info)
{
	double units_power = rapl->units_power;
	double units_time = rapl->units_time;
	uint64_t result;

	// power info
	result            = msr_read(rapl->msr, MSR_PKG_POWER_INFO);
	info->tdp         = units_power * (double)(result & 0x7fff);
	info->power_min   = units_power * (double)((result >> 16) & 0x7fff);
	info->power_max   = units_power * (double)((result >> 32) & 0x7fff);
	info->time_window = units_time  * (double)((result >> 48) & 0x7fff);

	// power limits
	result               = msr_read(rapl->msr, MSR_PKG_RAPL_POWER_LIMIT);
	info->power_limit_1  = units_power * (double)((result >> 0) & 0x7FFF);
	info->time_window_1  = units_time  * (double)((result >> 17) & 0x007F);
	info->power_limit_2  = units_power * (double)((result >> 32) & 0x7FFF);
	info->time_window_2  = units_time  * (double)((result >> 49) & 0x007F);
	info->pl_lock        = !!(result >> 63);
	info->pl1_enable     = !!(result & (1ULL << 15));
	info->cl1_enable     = !!(result & (1ULL << 16));
	info->pl2_enable     = !!(result & (1ULL << 47));
	info->cl2_enable     = !!(result & (1ULL << 48));

	// dram
	result                 = msr_read(rapl->msr, MSR_DRAM_POWER_INFO);
	info->dram.tdp         = units_power * (double)(result & 0x7fff);
	info->dram.power_min   = units_power * (double)((result >> 16) & 0x7fff);
	info->dram.power_max   = units_power * (double)((result >> 32) & 0x7fff);
	info->dram.time_window = units_power * (double)((result >> 48) & 0x7fff);
}

void rapl_read(const rapl_package* rapl, rapl_state* state)
{
#if 0
	const double units_time   = rapl->units_time;
#endif
	const double units_energy = rapl->units_energy;
	uint64_t result;

	// Package energy
	result = msr_read(rapl->msr, MSR_PKG_ENERGY_STATUS);
	state->energy_pkg = units_energy * (double)result;

	result = msr_read(rapl->msr, MSR_PP0_ENERGY_STATUS);
	state->energy_pp0 = units_energy * (double)result;

	// Model-specific features.
	switch(rapl->cpu_model)
	{
#if 0
	// Bridge-EP
	case CPU_SANDYBRIDGE_EP:
	case CPU_IVYBRIDGE_EP:
		result = msr_read(msr, MSR_PKG_PERF_STATUS);
		status->pkg_acc_throttled_time = units_time * (double)result;

		result = msr_read(msr, MSR_PP0_PERF_STATUS);
		status->pp0_acc_throttled_time = units_time * (double)result;
		break;
#endif
	case CPU_SANDYBRIDGE:
	case CPU_IVYBRIDGE:
	case CPU_HASWELL:
		result = msr_read(rapl->msr, MSR_PP1_ENERGY_STATUS);
		state->energy_pp1 = units_energy * (double)result;
#if 0
			result = msr_read(pkg->msr, MSR_PP1_POLICY);
			pkg->pp1_policy = (uint32_t)(result & 0x001f);
#endif
		break;
	default:
		state->energy_pp1 = 0.0;
		break;
	}

	// DRAM
	switch(rapl->cpu_model)
	{
	case CPU_SANDYBRIDGE_EP:
	case CPU_IVYBRIDGE_EP:
	case CPU_HASWELL:
		result = msr_read(rapl->msr, MSR_DRAM_ENERGY_STATUS);
		state->energy_dram = units_energy * (double)result;
		break;
	default:
		state->energy_dram = 0.0;
		break;
	}
}

static inline uint32_t extract_temperature(uint64_t v)
{
	// read temp if valid reading.
	return v & (1 << 31) ? (v >> 16) & 0x7f : 0;
}

uint32_t rapl_temperature(const rapl_package* rapl)
{
	uint64_t result = msr_read(rapl->msr, MSR_PACKAGE_THERM_STATUS);
	return extract_temperature(result);
}

uint32_t rapl_temperature_ia32(const rapl_package* rapl)
{
	uint64_t result = msr_read(rapl->msr, MSR_IA32_THERM_STATUS);
	return extract_temperature(result);
}
