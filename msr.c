#define _XOPEN_SOURCE 700 // for pread
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include "msr.h"

msr_handle msr_open(uint32_t cpu)
{
	static char path[64];
	msr_handle fd = -1;

	snprintf(path, 64, "/dev/cpu/%u/msr", cpu);
	fd = open(path, O_RDONLY);

	if(fd < 0)
	{
		if(errno == ENXIO)
		{
			fprintf(stderr, "rdmsr: No CPU %u\n", cpu);
			exit(2);
		}
		else if(errno == EIO)
		{
			fprintf(stderr, "rdmsr: CPU %u doesn't support MSRs\n", cpu);
			exit(3);
		}
		else
		{
			perror("rdmsr:open");
			fprintf(stderr, "Trying to open %s\n", path);
			exit(127);
		}
	}

	return fd;
}

void msr_close(msr_handle fd)
{
	if(fd >= 0) close(fd);
}

uint64_t msr_read(msr_handle fd, off_t which)
{
	uint64_t value;
	if ( pread(fd, &value, sizeof(value), which) != sizeof(value) )
	{
		perror("rdmsr:pread");
		return -1;
	}

	return value;
}

int msr_write(msr_handle fd, off_t which, uint64_t value)
{
	if( pwrite(fd, &value, sizeof(value), which) != sizeof(value) )
	{
		perror("rdmsr:pwrite");
		return -1;
	}

	return 0;
}
