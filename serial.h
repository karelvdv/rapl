#pragma once

#define SERIAL_INVALID (-1)

typedef int serial_handle;

serial_handle serial_open(const char* device);
int serial_close(const serial_handle h);
int serial_setrts(const serial_handle h, int level);
