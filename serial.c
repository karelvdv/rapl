#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "serial.h"

serial_handle serial_open(const char* device)
{
	serial_handle h;
	if( (h = open(device, O_RDONLY)) == SERIAL_INVALID)
	{
		perror("serial_open(): open");
	}

	return h;
}

int serial_close(const serial_handle h)
{
	if(h != SERIAL_INVALID) close(h);
	return 0;
}

int serial_setrts(const serial_handle h, int level)
{
	int flg = TIOCM_RTS;
	int cmd = level ? TIOCMBIS : TIOCMBIC;
	return ioctl(h, cmd, &flg);
}
